from flask import render_template,redirect

from apps.home import home

@home.route('/')
def index():
    return redirect("/")
