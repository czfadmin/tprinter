from flask_debugtoolbar import DebugToolbar
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Shell, Server
from flask import render_template

from apps import CREATEAPP, db
from models import User

app = CREATEAPP()
manager = Manager(app)
migrate= Migrate(app,db)

def make_shell_context():
    return dict(app=app, db=db,User=User)


manager.add_command('db',MigrateCommand)
manager.add_command('shell',Shell(make_context=make_shell_context))
manager.add_command("server",Server())

if __name__ == "__main__":
    manager.run()