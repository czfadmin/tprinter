from flask import render_template,redirect,flash,request
from flask_login import login_required
from apps.user import user

@user.route("/<name>/profile")
@login_required
def profile(name):
    return render_template("/user/profile.html")

@user.route('/<name>/history')
@login_required
def history(name):
    name= name.strip(" ")
    return render_template('/user/history.html')

@user.route('/<name>/edit_profile')
@login_required
def edit_profile(name):
    name= name.strip(" ")
    return render_template('/user/edit_profile.html')
