from flask_wtf import FlaskForm, RecaptchaField
from flask_uploads import UploadSet, IMAGES
from wtforms import StringField, BooleanField, TextField, PasswordField, Form, validators
from wtforms.validators import DataRequired
from flask_wtf.file import FileField, FileRequired, FileAllowed

images = UploadSet('images', IMAGES)


class UploadPhotoForm(Form):
    photo = FileField(validators=[FileRequired()])


class UploadFileForm(Form):
    upload = FileField('image', validators=[
        FileRequired(),
        FileAllowed(images, 'Images only!')
    ])


# 不要使用FlaskForm
class LoginForm(Form):
    username = TextField('用户姓名', [validators.Length(min=4, max=25)], render_kw={
                         'class': 'form-control', 'placeholder': '输入用户名'})
    password = PasswordField('输入密码', [
        validators.Required(),
    ], render_kw={
        'class': 'form-control', 'placeholder': '输入密码'
    })
    remember_me=BooleanField("保持登录",render_kw={
        'class':'form-check-input',
        'id':'dropdownCheck2'
    })


class RegistrationForm(Form):
    username = TextField('用户姓名', [validators.Length(min=4, max=25)], render_kw={
                         'class': 'form-control', 'placeholder': '输入用户名'})
    email = TextField('邮箱地址', [validators.Length(min=6, max=35)], render_kw={
        'class': 'form-control', 'placeholder': '输入邮箱'})
    password = PasswordField('输入密码', [
        validators.Required(),
        validators.EqualTo('confirm', message='两次密码不匹配')
    ],
        render_kw={
        'class': 'form-control', 'placeholder': '输入密码'})
    confirm = PasswordField('确认密码',    [
        validators.Required(),
        validators.EqualTo('confirm', message='两次密码不匹配')
    ], render_kw={
        'class': 'form-control', 'placeholder': '确认密码'
    })
    # recaptcha=RecaptchaField()
    accept_tos = BooleanField('我接受相关的协议', [validators.Required()],)
