import os

from flask import render_template, redirect, url_for, flash, request, session, current_app
from werkzeug.utils import secure_filename
from flask_login import login_user, logout_user, login_required
# from flask_uploads

from apps.auth import auth
from apps.auth.forms import RegistrationForm, LoginForm
from models import User, Photo
from apps import db, login_manager

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'docx'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@auth.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        name = request.form['username']
        password = request.form['password']
        u = User.query.filter_by(username=name).first()
        if u is None:
            error = "无此用户"
            flash(error, 'error')
        elif not u.verify_password(password):
            error = "密码错误"
            flash(error, 'error')
        else:
            # session['logged_in'] = True
            login_user(u)
            flash('登录成功', 'success')
            current_app.logger.info('%s 登录成功', u.username)
            return redirect(request.args.get("next") or url_for('home.index'))
    return render_template("login.html", form=form)


@auth.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        name = request.form['username']
        email = request.form['email']
        password = request.form['password']
        u = User.query.filter_by(username=name).first()
        u0 = User.query.filter_by(email=email).first()
        if u is not None:
            error = "该用户名已被使用"
            flash(error, 'error')
        elif u0 is not None:
            error = "该邮箱已被注册"
            flash(error, 'error')
        else:
            db.session.add(User(name, email, password))
            db.session.commit()
            if User.query.filter_by(username=name) is not None:
                flash('Thanks for registering', 'success')
                current_app.logger.info('%s 注册成功', name)
                return redirect(url_for('auth.login'))
    return render_template("register.html", form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    # session.pop('logged_in')
    flash('你已经退出', 'success')
    current_app.logger.info('用户已退出')
    return redirect(url_for('home.index'))


@auth.route('/upload', methods=['GET', 'POST'])
@login_required
def upload():
    if request.method == 'POST':
            # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            c=os.getcwd()
            p=c+r'\apps\static\uploads'
            file.save(os.path.join(
                p, filename))
            return redirect(url_for('auth.uploaded',
                                    filename=filename))
    return render_template("/uploads/uploads.html")


@auth.route('/uploaded/<filename>', methods=['GET', 'POST'])
@login_required
def uploaded(filename):
    if filename.split(".")[-1] == "jpg" or filename.split(".")[-1] == "png":
        return render_template("/uploads/uploaded_image.html", filename=filename)
    elif filename.split('.')[-1] == "txt":
        return render_template("/uploads/uploaded_file.html", filename=filename)


@auth.route('/photo/<id>')
@login_required
def show(id):
    pass
