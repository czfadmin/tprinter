import logging
import time
import os
from logging.config import dictConfig
from flask.logging import default_handler

from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
from flask_moment import Moment
from flask import request

from config import DevConfig


# log配置，实现日志自动按日期生成日志文件
def make_dir(make_dir_path):
    path = make_dir_path.strip()
    if not os.path.exists(path):
        os.makedirs(path)
    return path


db = SQLAlchemy()
bootstrap = Bootstrap()
moment = Moment()
login_manager = LoginManager()
login_manager.session_protection = "strong"
login_manager.login_view = "auth.login"


class RequestFormatter(logging.Formatter):
    def format(self, record):
        record.url = request.url
        record.remote_addr = request.remote_addr
        return super().format(record)


def CREATEAPP():
    from models import User
    from apps.zfuadmin import zfuadmin as zfuadmin_blueprint
    from apps.auth import auth as auth_blueprint
    from apps.home import home as home_blueprint
    from apps.user import user as user_blueprint
    from apps.help import helper as helper_blueprint

    # log_dir_name = "logs"
    # log_file_name = 'logger-' + \
    #     time.strftime('%Y-%m-%d', time.localtime(time.time())) + '.log'
    # log_file_folder = os.path.abspath(os.path.join(os.path.dirname(
    #     __file__), os.pardir, os.pardir)) + os.sep + log_dir_name
    # make_dir(log_file_folder)
    # log_file_str = log_file_folder + os.sep + log_file_name
    # log_level = logging.WARNING
    # handler = logging.FileHandler(log_file_str,mode='a', encoding='UTF-8',delay=False)
    # handler.setLevel(log_level)
    # logging_format = logging.Formatter(
    #     '%(asctime)s - %(levelname)s - %(filename)s - %(funcName)s - %(lineno)s - %(message)s')
    # handler.setFormatter(logging_format)

    formatter = RequestFormatter(
        '[%(asctime)s] %(remote_addr)s requested %(url)s\n'
        '%(levelname)s in %(module)s: %(message)s'
    )
    # dictConfig({
    #     'version': 1,
    #     'formatters': {'default': {
    #         'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    #     }},
    #     'handlers': {'wsgi': {
    #         'class': 'logging.StreamHandler',
    #         'stream': 'ext://flask.logging.wsgi_errors_stream',
    #         'formatter': 'default'
    #     }},
    #     'root': {
    #         'level': 'INFO',
    #         'handlers': ['wsgi']
    #     }
    # })

    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///E:/Projects/PythonProjects/flask/tprinter/user.db'
    default_handler.setFormatter(formatter)
    # app.logger.addHandler(handler)
    db.init_app(app)
    bootstrap.init_app(app)
    login_manager.init_app(app)
    moment.init_app(app)
    app.config.from_object(DevConfig)

    @app.route('/')
    def index():
        return render_template("welcome.html")

    @app.errorhandler(404)
    def page_not_found(e):
        return render_template('404.html'), 404

    @app.errorhandler(403)
    def page_forbidden(e):
        return render_template('403.html'), 403

    @app.errorhandler(500)
    def internal_server_error(e):
        return render_template('500.html'), 500

    @app.errorhandler(403)
    def page_has_gone(e):
        return render_template('404.html'), 403

    @app.before_first_request
    def before_first_request():
        from models import Role, User
        app.logger.info('删除数据库中已存在的表')
        db.drop_all()
        app.logger.info('在数据库创建Role和User表')
        db.create_all()
        Role.insert_roles()
        User.insert_users()
        app.logger.info('Befor First Request Successfully!')

    @login_manager.user_loader
    def load_user(userid):
        return User.get(userid)

    # register bule_print
    app.register_blueprint(zfuadmin_blueprint, url_prefix="/admin")
    app.register_blueprint(home_blueprint, url_prefix="/home")
    app.register_blueprint(user_blueprint, url_prefix="/user")
    app.register_blueprint(auth_blueprint, url_prefix="/auth")
    app.register_blueprint(helper_blueprint, url_prefix="/help")

    return app
