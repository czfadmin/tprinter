import datetime

from flask_login import UserMixin, AnonymousUserMixin
from flask import Flask
from werkzeug.security import generate_password_hash, check_password_hash

from apps import db


class Permission(object):
    FOLLOW = 0x01
    COMMENT = 0x02
    WRITE_ARTICLES = 0x04
    MODERATE_COMMENTS = 0x08
    ADMINISTER = 0x80


class Role(db.Model):
    __tablename__ = "roles"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    Permission = db.Column(db.Integer)
    users = db.relationship('User', backref='role', lazy='dynamic')

    @staticmethod
    def insert_roles():
        roles = {
            'User': (Permission.FOLLOW | Permission.COMMENT | Permission.WRITE_ARTICLES, True),
            'User': (Permission.FOLLOW | Permission.COMMENT | Permission.WRITE_ARTICLES, True),
            'Administrator': (0xfff, False)
        }
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.Permission = roles[r][0]
            role.default = roles[r][1]
            db.session.add(role)
        db.session.commit()

    def __repr__(self):
        return '<Role %r>' % self.name


class User(db.Model,UserMixin):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(80), unique=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    register_time = db.Column(db.DateTime(), default=datetime.datetime.now)
    _last_login_time = db.Column(db.DateTime(), default=datetime.datetime.now)
    _password_hash = db.Column(db.String(240))
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))

    def __init__(self, username, email, password,role=None):
        self.username = username
        self.email = email
        self.password = password
        if role is None:
            if self.role is None:
                self.role= Role.query.filter_by(default=True).first()

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self._password_hash = generate_password_hash(password)

    @staticmethod
    def insert_users():
        users=[
            User('zfuChen',"2849007069@qq.com","123456"),
            User('hhChen',"8741069@qq.com","123456"),
            User('Tom',"9932169@qq.com","123456"),
            User('Jack',"665307069@qq.com","123456"),
            User('Mika',"219900@qq.com","123456"),
            User('Justin',"531134069@qq.com","123456"),
            User('Catter',"333369@qq.com","123456"),
            User('Java',"119rwer@qq.com","123456"),
            User('Python',"7rwer9@qq.com","123456"),
            User('Linux',"61rqwer@qq.com","123456"),
            User('Window',"2233qewer@qq.com","123456"),
        ]
        for u in users:
            db.session.add(u)
        db.session.commit()
        

    @property
    def LastLoginTime(self):
        return self.last_login_time

    @LastLoginTime.setter
    def LastLoginTime(self, value):
        if value is None:
            self._last_login_time = datetime.datetime.now()
        self._last_login_time = value
    
    def verify_password(self, password):
        return check_password_hash(self._password_hash, password)

    def can(self,Permission):
        return self.role is not None and (self.role.Permission & Permission)==Permission

    def is_administrator(self):
            return self.can(Permission.ADMINISTER)
    @staticmethod
    def get(id):
        return User.query.filter_by(id= id).first()
        
    def __repr__(self):
        return '<User %r>' % self.username


class AnnoymousUser(AnonymousUserMixin):
    def can(self,Permission):
        return False
    
    def is_administrator(self):
        return False

class Photo(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(80), unique=True)


    def __repr__(self):
        return ("<Photo {},id,{}>").format(self.name,self.id)