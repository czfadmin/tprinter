class Config:
    SECRET_KEY="The Config "
    def __init__(self):
        pass

    def __str__(self):
        return "Base Configure"


class DevConfig(Config):
    DEBUG = True
    SECRET_KEY="The Dev Environment Configure"
    UPLOAD_FOLDER="/uploads"
    MAX_CONTENT_LENGTH=8*1024*1024
    # SQLALCHEMY_DATABASE_URI="sqlite:///data.sqlite"
    def __str__(self):
        return "Development Configure"
class ProConfig(Config):
    DEBUG = False
    SECRET_KEY="The Production  Environment Configure "
    def __str__(self):
        return "Production Configure"