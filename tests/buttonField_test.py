import unittest
from flask_wtf import  Form

from CustomWidget import ButtonField


class TestButtonField(Form):
    choice = ButtonField('example')


class ButtonFieldTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        return super().tearDown()


if __name__ == "__main__":
    unittest.main()
